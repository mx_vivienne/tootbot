# tootbot

A bot that can read an archive of a Mastodon posts, and create new nonsense posts using a markov chain.

# Dependencies

Mastodon.py

# Usage

First, download your archive, extract the outbox.json and build the markov chain with:\
`./bot.py build path/to/your/outbox.json`

Now you can let it generate new posts, optionally specifying the first word, with:\
`./bot.py post [-s word]`

To update the chain, just build again with a newer outbox.json.
If you have multiple accounts, you can also combine them by listing all the outbox files with the build command.

For more information, see `./bot.py build --help` or `./bot.py post --help`
