#!/usr/bin/python3

import sys, json, re, argparse
from random import randrange
from pathlib import Path
from shutil import copyfile
from os import fspath
from parser import *

re_whitespace = re.compile(r"[\u200B\s]+")
re_trailing_punctuation = re.compile(r"[^a-zA-Z*:_~`\")]+$")
re_follower_address     = re.compile("http\S*?/followers(?: |$)")

re_comment1 = re.compile(r"\s//.*?$", re.MULTILINE)
re_comment2 = re.compile(r"/\*.*?\*/", re.DOTALL)

confdir = Path("~/.config/tootbot").expanduser()
markov_path = fspath(confdir/"markov.json")

spacing_table_order = ["tag open", "pair open", "tag close", "pair close",
        "punct eq", "punct neq", "word"]

def do_toots(markov, start):
    position = markov["data"]["global"][start]

    tagstack = ["global"]
    wordstack = [start]

    prevtype = "string open"

    toot = ""
    if (start != STRING_START):
        toot += start + " "

    while (True):
        #select next word
        number = randrange(position["total"])
        word = ""
        currtype = "word"

        for key,value in position["next_words"].items():
            word = key
            number -= value
            if (0 > number): break

        wordstack[-1] = word

        # special cases
        match = re_special.fullmatch(word)
        if (match):
            mode,kind = match.group(1),match.group(2)

            if ("place" == mode):
                name = match.group(3)
                tagstack.append(name)
                if ("tag" == kind):
                    word = "<" + name + ">"
                    currtype = "tag open"
                    wordstack.append(TAG_OPEN.format(name))
                else:
                    word = name
                    currtype = "pair open"
                    wordstack.append(PAIR_OPEN.format(name))

            elif ("close" == mode):
                if ("string" == kind):
                    return toot.rstrip()
                elif ("tag" == kind):
                    currtype = "tag close"
                    word = "</" + tagstack[-1] + ">"
                elif (tagstack[-1] in quotes):
                    currtype = "pair close"
                    word = tagstack[-1]
                else:
                    currtype = "pair close"
                    word = closing_chars[opening_chars.index(tagstack[-1])]
                tagstack.pop()
                wordstack.pop()

        # punctuation characters
        elif (re_punct_word.fullmatch(word)):
            if (len(word) == word.count(word[0])):
                currtype = "punct eq"
            else:
                currtype = "punct neq"

        # do we need to insert a space first?
        if (config["contents"]["spacing"][
            prevtype][spacing_table_order.index(currtype)]):
            toot += " "

        toot += word
        
        prevtype = currtype
        position = markov["data"][tagstack[-1]][wordstack[-1]]

def get_post_visibility(obj):
    p = "activitystreams#public"
    to, cc = " ".join(obj["to"]), " ".join(obj["cc"])
    if (p in to): return "public"
    if (p in cc): return "unlisted"
    if (re_follower_address.search(to)): return "followers-only"
    return "direct"

parser = argparse.ArgumentParser("tootbot")
subparsers = parser.add_subparsers(dest="mode")

#create parser for "build" command
parser_build = subparsers.add_parser("build")
parser_build.add_argument("input", nargs="*", type=argparse.FileType("r"), default=sys.stdin,
        help="arbitrary amount of outbox files to read, defaults to stdin if none specified")
parser_build.add_argument("-o", "--output", nargs="?", type=str, default=markov_path,
        help="file to write the markov chain to, in json format, defaults to stdout")
parser_build.add_argument("-n", "--indent", nargs="?", type=int, default=None, const=2,
        help="enables pretty printing, with an indentation of 2 spaces if no value is specified")

#create parser for "post" command
parser_post = subparsers.add_parser("post")
parser_post.add_argument("input", nargs="?", type=str, default=markov_path,
        help="json file containing the markov chain to use, defaults to ~/.config/tootbot/markov.json")
parser_post.add_argument("-o", "--output", nargs="?", type=argparse.FileType("w"), default=sys.stdout,
        help="file to write the generated posts to, defaults to stdout")
parser_post.add_argument("-s", "--start", nargs="?", type=str, default=STRING_START,
        help="specifies a word to start the post with")
parser_post.add_argument("-a", "--amount", nargs="?", type=int, default=1, help="specifies the number of posts to generate, 1 by default")
args=parser.parse_args()

# first make sure the config file and directories exist
confdir.mkdir(parents=True, exist_ok=True)
conffile = confdir / "config.json"
if (not conffile.exists()): copyfile("sample_config", conffile)

# parse config file
with conffile.open() as cf:
    configstr = cf.read()
configstr = re_comment1.sub("", configstr)
configstr = re_comment2.sub("", configstr)
config = json.loads(configstr)
del configstr

# read post archive and build markov chain
if ("build" == args.mode):
    parser = Parser()
    parser.init_parser()

    for file in args.input:
        outbox = json.load(file)
        toots = [toot.get("object") for toot in outbox.get("orderedItems")]
        post_rules = config["posts"]

        for toot in toots:
            if (type(toot) is dict and 
                    post_rules[get_post_visibility(toot)] and (
                    post_rules["sensitive"] or not toot.get("sensitive"))):
                parser.reset_parser()
                parser.feed(toot.get("content"))
                parser.close()

        with open(args.output, "w") as output:
            json.dump(parser.markov, output, indent=args.indent)

# read markov chain json and generate posts
elif ("post" == args.mode):
    with open(args.input, "r") as infile:
        markov = json.load(infile)
    for i in range(args.amount):
        args.output.write(do_toots(markov, args.start) + "\n")
        if (i + 1 < args.amount):
            args.output.write("\n" + "-" * 10 + "\n\n")
