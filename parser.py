from html.parser import HTMLParser
import re

STRING_START =  "__open_string__"
STRING_END   = "__close_string__"

# use with .format("blockname")
TAG_OPEN =   "__open_tag_{0}__"
TAG_CLOSE = "__close_tag_{0}__"
TAG_PLACEHOLDER = "__place_tag_{0}__"

PAIR_OPEN  =  "__open_pair_{0}__"
PAIR_CLOSE = "__close_pair_{0}__"
PAIR_PLACEHOLDER = "__place_pair_{0}__"

#re_punctuation = re.compile(r"((?<!\w))?[^\w\s@]+(?(1)|(?!\w))")
re_punctuation = re.compile(r"((?<!\w)|^)?[^\w\s@:]+((?(1)|(?!\w))|$)")
re_punct_word = re.compile(r"[^a-zA-Z0-9]+")
re_shortcode = re.compile(r":\w+:")
re_colon = re.compile(r":")
re_word = re.compile(r"[\S]+")
re_mention_href = re.compile(r"https?://(.*?)/(@.*)")
re_special = re.compile(
        r"__(open|close|place)_(string|tag|pair)(?:_([^\s_]+))?__")
re_alnum = re.compile(r"\w+")

# quotes are characters that can both open and close a block
opening_chars = "([{<"
closing_chars = ")]}>"
quotes = "'\""
re_delim_char = re.compile("(?<= )[" + re.escape("%s" % 
        (opening_chars + closing_chars + quotes)) + "](?= )")

#character_pairs = {
#    "(": ")",
#    "[": "]",
#    "{": "}",
#    "<": ">",
#    '"': '"',
#    "'": "'"
#}

allowed_tags = [
    "h1", "h2", "h3", "h4", "h5", "h6",
    "strong", "em", "u", "del",
    "blockquote", "pre", "code",
    "sub", "sup", "ol", "ul", "li"
]

# first do .init_parser()
# then use .feed("input") to start parsing
# call .close() after .feed()
# call .reset_parser if u want to parse more inputs
class Parser(HTMLParser):
    def init_parser(self):
        self.reset()
        self.reset_parser()
        self.markov = {"meta": {}, "data": {}}

    def reset_parser(self):
        self.tagstack = ["global"]
        self.wordstack = [STRING_START]
        self.readinglink = False

    def handle_starttag(self, tag, attrs):
        if (self.readinglink): return
        attrs = dict(attrs)

        # links and mentions
        if ("a" == tag):
            self.readinglink = True

            if ("class" in attrs and "href" in attrs and
                    "mention" in attrs["class"]):
                match = re_mention_href.search(attrs["href"])
                if (match):
                    user = match.group(2) + "@" + match.group(1)
                    self.increment_count(self.tagstack[-1],
                            self.wordstack[-1], user)
                    self.wordstack[-1] = user

        # custom emoji
        #elif ("img" == tag and "class" in attrs and "title" in attrs and
        #        "custom-emoji" in attrs["class"]):
        #    emoji = attrs["title"]
        #    self.increment_count(self.tagstack[-1],
        #            self.wordstack[-1], emoji)
        #    self.wordstack[-1] = emoji

        # usual formatting tags
        elif (tag in allowed_tags):
            self.increment_count(self.tagstack[-1],
                    self.wordstack[-1], TAG_PLACEHOLDER.format(tag))
            self.wordstack[-1] = TAG_PLACEHOLDER.format(tag)
            self.wordstack.append(TAG_OPEN.format(tag))
            self.tagstack.append(tag)

    def handle_endtag(self, tag):
        if (self.readinglink):
            self.readinglink = "a" != tag

        elif ("p" == tag):
            self.increment_count(self.tagstack[-1],
                    self.wordstack[-1], "\n\n")
            self.wordstack[-1] = "\n\n"

        elif (tag in allowed_tags):
            # find last occurence of this tag in tagstack
            pos = -1
            for i in range(len(self.tagstack)-1, -1, -1):
                if (self.tagstack[i] == tag):
                    pos = i
                    break
            if (-1 != pos): self.close_tags(pos)
                
    def handle_data(self, data):
        if (self.readinglink): return

        # find all colons not part of shortcodes
        colons = [x.start() for x in re_colon.finditer(data)]
        for word in re_shortcode.finditer(data):
            if (word.end()-1 in colons): colons.remove(word.end()-1)
            if (word.start() in colons): colons.remove(word.start())

        # insert spaces around these colons
        for i in sorted(colons, reverse=True):
            data = data[:i] + " " + data[i] + " " +  data[i+1:]

        # insert spaces around runs of punctuation characters
        data = re_punctuation.sub(lambda x: " "+x.group(0)+" ", data)

        # find all potential delimiter characters
        delims = [(x.group(0), x.start()) for x in 
                re_delim_char.finditer(data)]

        # find pairs, then put sequence into chain
        pairs = sorted(self.find_pairs(delims), reverse=True)
        self.do_word_counts(data, pairs)

    def close(self):
        if (1 < len(self.tagstack)): self.close_tags(1)
        self.increment_count(self.tagstack[0], self.wordstack[0],
                STRING_END)
        super().close()

    def close_tags(self, pos):
        # close blocks and increment counts accordingly
        # Im assuming here that pairs are always being closed properly,
        # bc find_pairs does not mark unmatched characters
        for i in range(len(self.tagstack)-1, pos-1, -1):
            self.increment_count(self.tagstack[-1],
                self.wordstack[-1], 
                TAG_CLOSE.format(self.tagstack.pop()))
            self.wordstack.pop()

    def increment_count(self, scope, first, second):
        data = self.markov["data"]

        if (scope not in data): data[scope] = {}

        if (first not in data[scope]):
            data[scope][first] = {"total": 0, "next_words": {}}
        data[scope][first]["total"] += 1

        if (second not in data[scope][first]["next_words"]):
            data[scope][first]["next_words"][second] = 0
        data[scope][first]["next_words"][second] += 1

    def do_word_counts(self, data, pairs):
        closers = []
        nextpair = None
        if (0 < len(pairs)): 
            nextpair = pairs.pop()

        for word in re_word.finditer(data):
            # is this an opener?
            if (nextpair and word.start() == nextpair[1]):
                char = word.group(0)
                self.increment_count(self.tagstack[-1],
                        self.wordstack[-1], 
                        PAIR_PLACEHOLDER.format(char))
                self.wordstack[-1] = PAIR_PLACEHOLDER.format(char)
                self.wordstack.append(PAIR_OPEN.format(char))
                self.tagstack.append(char)
                closers.append(nextpair[1])

            # is this a closer?
            elif (nextpair and 0 < len(closers) 
                    and word.start() == closers[-1]):
                char = word.group(0)
                self.increment_count(self.tagstack[-1],
                    self.wordstack[-1], 
                    PAIR_CLOSE.format(char))
                self.tagstack.pop()
                self.wordstack.pop()
                closers.pop()

            # just a regular word
            else:
                content = word.group(0)
                self.increment_count(self.tagstack[-1],
                        self.wordstack[-1], content)
                self.wordstack[-1] = content

    def find_pairs(self, delims):
        pairs = []
        closer = 0
        while (closer < len(delims)):
            opener = 0
            closechar = openchar = ""
    
            # find next potential closer
            while (delims[closer][0] not in (closing_chars + quotes)):
                closer += 1
                # if no closers found, return
                if (len(delims) <= closer): return pairs
    
            # find required opener
            closechar = delims[closer][0]
            if (closechar in quotes):
                openchar = closechar
            else:
                openchar = opening_chars[closing_chars.index(closechar)]
    
            # find first matching opener
            while (delims[opener][0] != openchar):
                opener += 1
                if (opener >= closer):
                    opener = -1
                    break
    
            # if no matching opener found, continue
            if (-1 == opener):
                # if it cant be an opener either, remove it
                if (closechar in quotes): closer += 1
                else: delims.pop(closer)
                continue
    
            # save pair and delete everything inbetween
            pairs.append((delims[opener][1], delims[closer][1]))
            del delims[opener:closer+1]
            closer = opener
        return pairs

